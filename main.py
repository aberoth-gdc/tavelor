"""
Main file for running the Tavelor bot
"""
#!/usr/bin/python

import logging
import os

import discord

import tavelor

logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

TOKEN: str = os.getenv('DISC_BOT_TOKEN')
logger.info(f"Discord token: {TOKEN}")

tavelor.client.run(TOKEN)
