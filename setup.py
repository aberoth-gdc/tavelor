from setuptools import setup


def readme():
    """
    Function to read the long description for the Tavelor package.
    """
    with open('README.md') as _file:
        return _file.read()


setup(
    name='tavelor',
    version='1.0.3',
    description="Library for launching a Tavelor bot",
    long_description=readme(),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/aberoth-gdc/tavelor',
    author='Twoshields',
    classifiers=[
        "Topic :: Software Development :: Libraries :: Python Modules"
    ],
    packages=['tavelor'],
    install_requires=['discord'],
    python_requires='>=3.9',
    zip_safe=False)
