"""
All modules for running a Tavelor Discord bot.
"""

import logging
import os

import discord
from discord.errors import Forbidden

from .commands import parse_command
from .util import get_discord_id, preprocess_input


logger = logging.getLogger(__name__)
logging.basicConfig()
logger.setLevel(logging.DEBUG)

client = discord.Client()


@client.event
async def on_ready():
    logger.info("===== TAVELOR BEGIN =====")

    logger.info("TAVELOR IS READY")
    logger.info("TAVELOR USER ID: {}".format(client.user.id))
    #TODO Set up list of guilds with permission to use bot
    #for guild in client.guilds:
    #    if guild.name == GUILD:
    #        break
    return


@client.event
async def on_message(message):
    logger.info(f"{message.guild}, #{message.channel}, {message.guild}: {message.content}")

    # Don't bother with Tavelor's messages
    if message.author == client.user:
        return

    if len(message.content) == 0:
        return

    # TODO: Handle the mobile mention case,e.g. <!{}> instead of <@!{}>
    # TODO: Handle with regexp
    # NOTE: We do this before preprocessing to save time and resources, don't
    # handle when we're not mentioned
    if get_discord_id(message.content.split()[0]) != client.user.id:
        return

    # TODO: Strip some characters
    # TODO: Implement regexp matching instead of array match
    # TODO: Handle Main
    # TODO: Handle Guild
    # TODO: Handle Schools and Skills

    cmd = parse_command(message)
    msg: str = await cmd(message)

    if msg != "":
        logger.debug(msg)
        # Try to do a fancy reply, otherwise just send to channel.
        try:
            await message.reply(msg)
        except Forbidden:
            await message.channel.send(msg)


    #mention: str = message.author.mention

    #content: list[str] = preprocess_input(message.content)
    #logger.debug("Content after preprocessing: {}".format(content))

    #msg: str = "I'm not sure what you mean, {}.".format(mention)
    ## They only said our name.
    #if len(content) == 1:
    #    logger.info("Responding to mention")
    #    msg = "What can I do for you, {}?".format(mention)

    ## They said a topic
    #elif len(content) == 2:
    #    logger.info("Responding to trivia")
    #    # TODO: Import tavelor trivia
    #    if content[1].lower() in trivia:
    #        msg = trivia[content[1].lower()].format(mention)

    ##elif content[1] == 'who' or content[1] == "tell":
    ##    logger.info("Responding to a query")
    ##    if content[2] == "else" and len(content) >= 4 and is_id(content[3]):
    ##        msg = get_alts(content[3], mention)
    ##    elif is_id(content[2]):
    ##        msg = await identify(content[2], mention)
    ##    elif (len(content)>=5 and content[2] == "me" and content[3] == "about"
    ##          and is_id(content[4])):
    ##        msg = champion_info(content[4], message)
    ##elif content[2] == "is" and is_id(content[1]):
    ##    change = False
    ##    if content[-1] == "now":
    ##        logger.info("Got a request for change")
    ##        change = True

    ##    logger.info("Responding to {} action".format(
    ##        "change" if change else "addition"
    ##    ))

    ##    if content[3] == "in":
    ##        logger.info("Adding an Identity to a Guild")
    ##        msg = set_guild(content[1], " ".join(content[4:]), message)
    ##    if content[3] == "a":
    ##        if content[4] in ("enemy", "ally"):
    ##            msg = set_ident_status(content[1], content[4], message)
    ##        else:
    ##            logger.info("Setting magic school on an identity.")
    ##            msg = set_school(content[1], content[4].lower(), message)
    ##    #elif content[3] == "og" and is_id(content[4]):
    ##    #    logger.info("Setting og on an Identity")
    ##    #    msg = cmd.set_og(content[1], content[4], mention)
    ##    #elif content[3] in ("enemy", "ally"):
    ##    #    logger.info("Changing a status.")
    ##    #    msg = cmd.set_status(content[1], content[3])
    ##    elif is_id(content[3]):
    ##        logger.info("Adding new Champion Identity")
    ##        msg = add_user(content[1], content[3], mention)
    ##    # TODO Make this async, send the msg before we write
    ##    write_db()

    ## Try to do a fancy reply, otherwise just send to channel.
    #try:
    #    await message.reply(msg)
    #except Forbidden:
    #    await message.channel.send(msg)
