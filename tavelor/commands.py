"""
Commands that Tavelor bot uses to give responses.
"""

import logging
import typing

import discord

from .util import preprocess_input

from .database import get_identities_list, get_guilds_list
from .database import identities, guilds, champions
from .errors import InvalidIdError, UnknownIdentityError
from .models import Identity, schools
from .util import is_discord, get_discord_id, is_id, is_name


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

trivia: dict[str, str] = {
	"hi": "What can I do for you, {}?",
	"hello": "What can I do for you, {}?",
	"wazzup": "What can I do for you, {}?",
	"wassup": "What can I do for you, {}?",

	"bar": "My tavern is a place to relax and meet fellow adventurers.",
	"magic": "",
	"beer": "Excellent choice, {}. Grab one from the bar.",
	"c o": "b e...",
	"chicken": "Chickens are a great source of food.",
	"cleaning": "When you get to be my age, you end up with a lot of stuff " \
		"you don't even use anymore.",
	"darklow": "Darklow prefers to drink alone.",
	"death": "I'm sure you'll be prepared when the time comes, {}.",
	"egg": "Eggs are delicious, just don't throw them in the tavern!",
	"emerald": "One night a man in a cloak ordered a bottle of absinthe. He " \
		"placed a single emerald on the bar. I looked down to examine it, but" \
		" when I looked up the man and his drink were gone.",
	"forstyll": "The antics of Forstyll are legendary. He was quite the " \
		"mischief maker when he was younger.",
	"found": "If you find someone's stuff, bring it to me and I will keep it " \
		"safe for them for a few days.",
	"friend": "Perhaps one day you will encounter my old friend in you " \
		"travels.",
	"gamble": "",
	"hint": "I sell drinks, offer services, and always keep up on the town " \
		"gossip.",
	"inn": "My inn provides well deserved rest for weary adventurers.",
	"journal": "I keep a private journal. Future generations will want to " \
		"know what happened here.",
	"juice": "Sorry, I don't carry anything fancy. May I suggest a beer?",
	"life": "Life is full of surprises.",
	"lich": "",
	"lost": "If you lost something, go ahead and look through the lost and " \
		"found.",
	"milk": "Sorry, I don't carry anything fancy. May I suggest a beer?",
	"orc": "While terrifying to many, the orcs and their leader are simply " \
		"pawns in the world beyond this town.",
	"pet": "I once owned a wolf who gave his life defending me from orcs. " \
		"(sniff)",
	"pelt": "",
	"rabbit": "Thanks for asking, {}, but I have everything I need now.",
	"rum": "Sorry, I don't carry anything fancy. May I suggest a beer?",
	"satyr": "I like to listen to a satyr's music... from a distance.",
	"soda": "Sorry, I don't carry anything fancy. May I suggest a beer?",
	"stout": "My finest beer.",
	"table": "My work table is over there by the lost and found.",
	"tavern": "My tavern is a place to relax and meet fellow adventurers.",
	"times": "The world is becoming very dangerous. My fighting years are " \
		"behind me, but I have come to this place to help how I can.",
	"town": "I have travelled far. This is not the only town that has fallen " \
		"on troubled times.",
	"trouble": "The world is becoming very dangerous. My fighting years are " \
		"behind me, but I have come to this place to help how I can.",
	"water": "Sorry, I don't carry anything fancy. May I suggest a beer?",
	"work": "She does not realize it, but Inala is training an army of " \
		"champions, who's purpose is much greater than fighting rogues and " \
		"orcs.",
	"world": "I have travelled far. This is not the only town that has " \
		"fallen on troubled times.",
    "coffee": "Beer?",
    "elsewhere": "Come find me in my tavern, {}, I'll have more to say there.",
    "help": "I currently have no help documents, please ping Twoshields.",
    "who": "Come find me in my tavern, {}, I'll have more to say there.",

	"gomald": "Gomald is on our side, but perhaps only while it is profitable.",
	"lysis": "Lysis does not seem to like me, but I am not sure why.",
    "inala": "Inala has no idea how important her work is.",
    "sholop": "Sholop can be a bit thick at times.",

	"skaldor": "Legend has it Skaldor was a great",
}


def parse_command(message: discord.Message) \
		-> typing.Callable[[typing.Any], str]:
	content: list[str] = preprocess_input(message.content)
	if len(content) ==  1:
		return greeting
	if len(content) == 2 and content[1].lower() in trivia:
		return get_trivia

	return no_match


def not_authorized(message: discord.Message) -> str:
    """
    Give a message if the user tries to access a command from a Guild without
    sufficient permissions.
    """
    not_authorized_message: str = "Greetings, {}. I can't tell you about that."
    return not_authorized_message.format(message.author.mention)


async def no_match(message: discord.Message) -> str:
    """
    Provided when we don't know what they're talking about.
    """
    no_match_message: str = "I have not heard about that."
    return no_match_message.format(message.author.mention)


async def greeting(message: discord.Message) -> str:
    """
    They only said our name.
    """
    return trivia['hi'].format(message.author.mention)


async def get_trivia(message: discord.Message) -> str:
    """
    Return some trivia that you could get from talking to the NPC in game.
    Generally only one-word phrases
    """
    content: list[str] = preprocess_input(message.content)
    return trivia[content[1].lower()].format(message.author.mention)

def add_user(first: str, second: str, message) -> str:
    """
    TODO
    """
    discord: int = 0
    alts: list[str] = []
    patron = message.author.mention
    identity: Identity = None
    logger.info("add_user {} {}".format(first, second))

    if first == second:
        return "What a silly thing to say, {}!".format(patron)

    if is_discord(first) and is_discord(second):
        # TODO: Handle double Discords, for spies or something
        return "I can't keep all that in my head. Who's who?"

    # Now we need to figure out if we were given a Discord at all

    # If the OG is a Discord username, corrent our variables
    if is_discord(first):
        logger.debug("First argument was a discord user.")
        discord = get_discord_id(first)
        # Make sure we know the first input was a Discord ID
        first = ""
    else:
        alts.append(first)

    if is_discord(second):
        logger.debug("Second argument was a discord user.")
        discord = get_discord_id(second)
        # Make sure we know the second input was a Discord ID
        second = ""
    else:
        # If the OG was a discord username, we treat the alt as OG
        if first == "":
            first = second
            second = ""

        alts.append(second)

    msg: str = "Thanks, {}. I'll keep that in mind.".format(patron)

    assert isinstance(discord, int)

    # Check Discord first, since we give that preference
    if discord != 0 and discord in identities:
        identity = identities[discord]
        logger.info(
            "Found matching identity from discord: {}".format(identity.og)
        )

        if first in identity.alts:
            logger.debug("a I have {} identities.".format(
                len(get_identities_list())
            ))
            return "I think I already know that, {}".format(patron)

    if first in identities:
        if identity is not None:
            identity = identities[first]
        elif first not in identity.alts and first != identity.og:
            logger.debug("b I have {} users.".format(
                len(get_identities_list())
            ))
            return "I think their name is <@!{}>, {}".format(
                identity.discord,
                patron
            )

        # We've heard of the first, and we've heard of the second
        if second != "" and second in identities:
            # But they're different people
            if identity.og != identities[second].og:
                return "I think those are different people, {}".format(patron)

            return "I think I already know that, {}".format(patron)

    # We've never heard of first but we've heard of second
    if second != "" and second in identities:
        # first should be the one we know about, second is the addition
        first, second = (second, first)
        identity = identities[first]
    else:
        identity = Identity(first,discord,'','',alts)
        logger.debug("Adding user {}".format(identity))
        msg = "{} I've not heard of {} before..".format(msg, identity.alias)

    if first != "" and first not in identity.alts:
        identity.alts.append(first)

    if second != "" and second not in identity.alts:
        identity.alts.append(second)

    if int(discord) != 0 and int(identity.discord) == 0:
        identity.discord = discord

    if int(identity.discord) != 0:
        identities[identity.discord] = identity

    for alt in identity.alts:
        identities[alt] = identity

    logger.debug("Now have {} users.".format(len(get_identities_list())))

    return msg


# TODO: Need to find a way to NOT pass the discord client in...
async def identify(name, mention):
    """
    Returns a message giving info on the Identity of a name.
    """
    if not is_name(name):
        return "I only know citizens of Aberoth, {}".format(mention)

    if name not in identities:
        return "I don't know who {} is.".format(name.capitalize())

    identity = identities[name]

    if int(identity.discord) == 0:
        return "{} is {}.".format(name.capitalize(), identity.og.capitalize())

    msg = "{} is {}.".format(
		name.capitalize(), "<@!{}>".format(identity.discord)
	)

    if identity.guild != "":
        if guilds[identity.guild.lower()].leader.lower() in identity.alts:
            msg = "{} They are the leader of {}.".format(msg, identity.guild)
        else:
            msg = "{} They are a member of {}.".format(msg, identity.guild)

    return msg


def set_guild(name, guild_name, message):
	"""
	Sets the guild affiliation of an Identity.
	"""
	logger.debug("There are %d guilds loaded.", len(get_guilds_list()))

	mention = message.author.mention

	if not is_id(name):
		raise InvalidIdError(name)
	if guild_name not in guilds:
		logger.warning("%s not in guilds", guild_name)
		return f"I'm not sure I've heard of that guild, {mention}"

	guild = guilds[guild_name]

	if name not in identities:
		# TODO: Is there anything we can do here?
		return  f"I'm not sure I've heard that name before, {mention}"

	identity = identities[name]

	if identity.guild == guild.name:
		return f"I think I already knew that, {mention}"

	identity.guild = guild.name

	msg = f"Thank you for telling me, {mention}."

	return msg

#if guild.status == FRIENDLY:
#    if identity.status == ENEMY:
#        msg = "{} However, I'd still keep my eye on them..".format(msg)
#    elif identity.status == FRIENDLY:
#        msg = "{} It's been a long time coming for this.".format(msg)
#    else:
#        msg = "{} It's always nice to make new friends.".format(msg)
#    return msg
#if guild.status == ENEMY:
#    if identity.status == FRIENDLY:
#        return "{} It's a shame, I had high hopes for them.".format(msg)
#    if identity.status == NEUTRAL:
#        return f"{msg} It's a shame, " \
#			"I hope they know what they're getting into.."
#    if identity.status == ENEMY:
#        return "{} It was something I had seen coming. So be it.".format(msg)


def set_og(name, og, mention):
	if og == name:
		return f"I already know that, {mention}"

	if name not in identities:
		msg = add_user(name, "", mention)

	identity = identities[name]
	identity.og = og

	msg = f"Thank you, {mention}, I'll keep that in mind."

	ident_og = identities[og] if og in identities else None
	if ident_og is not None and ident_og.main != identity.main:
		if ident_og.og == og:
			# TODO: What to do here?
			ident_og.og = ""
		return f"{msg} It's a shame they gave that account to " \
			f"{ident_og.discord if ident_og.discord != 0 else ident_og.main}."

	return msg

def set_ident_status(name, status, mention):
	"""
	Sets the status of an Identity
	"""
	raise NotImplementedError()


def set_guild_alliance(name, alliance_name, mention, change=False):
	"""
	Adds a guild to an alliance
	"""
	raise NotImplementedError()


def get_alts(ident, mention):
	"""
	Returns a message about the Identity's other Champions
	"""
	logger.debug("get alts %s (%s) %s (%s)", str(ident), type(ident),
			  str(mention), type(mention))

	logger.debug("is id(%s): %s", ident, is_id(ident))
	if not is_id(ident):
		logger.debug("is id(%s): %s", str(ident), is_id(ident))
		raise InvalidIdError(ident)

	if ident not in identities:
		raise UnknownIdentityError(ident)

	identity = identities[ident]

	alts = [alt.capitalize() for alt in identity.alts]
	logger.debug(alts)
	alts_str = f"{', '.join(alts[:-1])}, and {alts[-1]}"

	if is_name(ident) and ident != identity.og:
		og_str = identity.og.capitalize()
		if identity.og.capitalize() in alts:
			alts.remove(og_str)
		alts_str = "{}, and {}".format(", ".join(alts[:-1]), alts[-1])
		msg = "They're normally known as {}, {}.".format(og_str, mention)
		msg = "{} They're also known as {}.".format(msg, alts_str)
		return msg

	return f"I've heard they also go by {alts_str}."


def set_school(name, school, message, change=False):
	mention = message.author.mention
	if not is_name(name):
		return f"Sorry, {mention}, only Champions can use magic."

	if school not in schools:
		return "I'm not familiar with that school of magic."

	if name not in champions:
		return f"I've not heard of a {name.capitalize()} " \
			"in the land of Aberoth.."

	champion = champions[name]

	if not change and champion.school != "":
		if champion.school == schools[school]:
			return f"I think I already know that, {mention}"

		return f"I've been told that {name.capitalize()} studies in the " \
			f"art of {champion.school.capitalize()}, has this changed?"

	champion.school = schools[school]

	msg = f"Thank you, {mention}, I'll keep that in mind."

	# TODO: Flavor text for each school.
	return msg


def champion_info(name, message):
	mention = message.author.mention
	if not is_name(name):
		return f"Sorry, {mention}, I can only tell you what I " \
			"know about citizens of Aberoth."

	if name not in champions:
		return f"I've not heard of a {name.capitalize()} " \
			"in the land of Aberoth.."

	champion = champions[name]
	identity = None
	if champion.name in identities:
		identity = identities[champion.name]

	if identity is None:
		msg = f"I haven't heard much about {name.capitalize()}. " \
			"However, I can tell you"
	else:
		msg = f"{champion.name.capitalize()} is also known by the name of"
		if int(identity.discord) != 0:
			msg = f"{msg} <@!{identity.discord}>."
		else:
			msg = f"{msg} {identity.og.capitalize()}."

	# TODO: Also search champion for guild info
	if identity is not None and identity.guild != "":
		they = "they"
		if msg[-1] == ".":
			they = "They"
		msg = "{msg} {they} are a member of " \
			"{' '.join([x.capitalize() for x in identity.guild.split()])}"

		if champion.school != "":
			msg = f"{msg}, and"
		else:
			msg = f"{msg}."

	if champion.school != "":
		they = "they"
		if msg[-1] == ".":
			they = "They"

		msg = f"{msg} {they} study in the school of " \
			"{champion.school.capitalize()}."

	msg = f"{msg} They have a skill total of {champion.skill_total}."

	return msg
