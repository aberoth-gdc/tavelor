"""
Utility functions for the Tavelor Discord bot
"""

import logging

import aberothutil as util

logger = logging.getLogger(__name__)
logger.setLevel(logging.INFO)


def preprocess_input(inp):
	"""
	Runs a client input through a series of checks and changes.
	"""
	output = []
	changed = False

	if isinstance(inp, str):
		inp = inp.split()

	for i, word in enumerate(inp):
		if isinstance(word, (float,int)):
			output.append(word)
			continue
		word = word.lower().replace('?', '')
		if util.is_discord(word):
			output.append(util.get_discord_id(word))
		elif word in ("who", "who's", "whos"):
			output.append("who")
		# Since we shorten who, get rid of the is
		elif word == "is":
			if output[i-1] in ("who", "else"):
				continue
			output.append(word)
		elif word == "":
			continue
		elif word in ("a", "an"):
			output.append("a")
		elif word in ("anymore", "now"):
			changed = True
			continue
		else:
			output.append(word)

	if changed:
		output.append("now")

	return output
