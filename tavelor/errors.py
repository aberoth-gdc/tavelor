#!/usr/bin/python


class InvalidIdError(Exception):
    def __init__(self, ident):
        self.ident = ident
        super().__init__("{} is not a valid Identity".format(self.ident))


class InvalidDiscordError(Exception):
    def __init__(self, discord):
        self.discord = discord
        super().__init__("{} is not a valid Discord ID".format(self.discord))


class InvalidNameError(Exception):
    def __init__(self, name):
        self.name = name
        super().__init__("{} is not a valid Aberoth name.".format(self.name))

class DuplicateDiscordError(Exception):
    def __init__(self, discord):
        self.discord = discord
        super().__init__("FOUND DUPLICATE DISCORD ID: {}".format(self.discord))

class UnknownIdentityError(Exception):
    def __init__(self, ident):
        self.ident = ident
        super().__init__("{} is not an Identity.".format(self.ident))
