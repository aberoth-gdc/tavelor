https://discord.js.org/#/
https://discordpy.readthedocs.io/en/latest/api.html

# Tavelor

Tavelor is a Discord bot for interacting with an Aberoth data collection engine.

It will include

- Detailed reputation information
- Last seen realm, date, time information
- Allegiance information, alts information

Possibly also trade information?

It all depends on the other Aberoth projects.

TODO: Create coverage report for known identities of Champions
TODO: Create integration tests for discord aspects
TODO: Create database tests
TODO: Finish util tests
